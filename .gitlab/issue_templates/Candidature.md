[//]: # (Indiquez en titre de l'issue « [candidature] » suivi du nom de votre chaton, par exemple « [candidature] chat-docks, services internet pour la marine marchande »)

[//]: # (merci d'utiliser le forum chatons - https://forum.chatons.org pour des retours/questions sur une entité en cours de création, les candidatures sont pour les structures remplissant déjà les pré-requis mentionés ci-dessous.)

## Description

Indiquez ici une description de votre chaton (vous pouvez reprendre celle que vous avez utilisée dans votre fiche sur chatons.org). Utilisez l'onglet « prévisualiser » ci-dessus pour vérifier que tout s'affiche correctement

## Liens
[//]: # (à compléter, utilisez la prévisualisation pour vérifier que vos liens s'affichent correctement)
[//]: # (ces informations sont obligatoires pour que la candidature soit considérée comme complète)

[//]: # (merci de fournir un lien vers un site 'vitrine' présentant la structure et les services proposés)
- votre site : https://...
- votre page de CGU : https://...
- votre fiche sur chatons.org...
- votre présentation sur leforum : 

[//]: # (ne rien modifier ci-dessous)
/label ~candidature
/due in six months
