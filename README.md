Coletivo de Hospedagens Alternativas Transparentes, Abertas (Open), Neutras e Solidárias (C.H.A.T.O.N.S.)
------------------------------------------------------------------------

<img alt="logo CHATONS" src="https://framagit.org/chatons/CHATONS/-/raw/master/docs/communication/logo/logo_chatons_v3.1.png" height="300" />

CHATONS é o Coletivo de Hospedagens Alternativas Transparentes, Abertas (Open), Neutras e Solidárias

Ele visa reunir estruturas que desejam evitar a coleta e a centralização de dados pessoais nos data centers do tipo que utilizam os GAFAM (Google, Apple, Facebook, Amazon, Microsoft).

CHATONS é um coletivo iniciado pela associação Framasoft logo após o sucesso de sua campanha « [Degooglisemos a Internet](https://degooglisons-internet.org) » .

O projeto visa reunir intervenientes que proponham serviços online livres, éticos, decentralizados e solidários a fim de permitir aos utilisadores de encontrar - rapidamente - alternativas aos produtos do Google (entre outros) que respeitem mais seus seus dados pessoais e a sua vida privada.

Para começar você pode:

  * [Descobrir os membros do coletivo](https://chatons.org/en/advanced-search?title=&field_revision_cp_localisation_value=&field_revision_type_infra_value=All&field_revision_type_structure_tid=All&field_revision_type_services_tid_op=and)
  * [Descobrir os serviços propostos](https://chatons.org/en/find-by-services)
  * E aprender mais sobre o projeto lendo o [FAQ - Questões Mais Frequentes](https://chatons.org/faq)
  * Ler os textos que fundaram o coletivo : seu [Manifesto](https://framagit.org/chatons/CHATONS/-/blob/master/docs/Manifesto-fr.md) e sua [Carta](https://framagit.org/chatons/CHATONS/-/blob/master/docs/Charte/Charter2-fr.md)
  * Conhecer [as ambições do coletivo](https://framablog.org/2016/02/09/chatons-le-collectif-anti-gafam/)
  * Descobrir como [ingressar no coletivo](https://chatons.org/rejoindre-le-collectif)
