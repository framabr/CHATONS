# Carta dos CHATONS

## O Coletivo

Chatons **C**oletivo de **H**ospedagens **A**lternativas **T**ransparentes, Abertas (**O**pen), **N**eutras e **S**olidárias. Cada membro do seu coletivo, abaixo denominado « CHATON », se compromete a respeitar a presente carta.
Aquilo que não é proibido explicitamente pela presente carta ou pela lei, é autorizado. 
O CHATON pode ser de qualquer forma jurídica : pessoa física ou pessoa jurídica (um particular, uma empresa, uma associação, uma SCOP - Sociedade Coperativa de Produção, etc )

A fim de ser ao máximo inclusivo, poupando diferentes sensibilidades sobre o assunto, o coletivo CHATONS redigiu esta carta aplicando as régras seguintes de escrita inclusiva :
- acordo quanto ao nome da função, nível hierárquico, profissões e títulos ;
- utilização de feminino e de masculino quando falamos de um grupo de pessoas na medida em que isso complique muito a frase (exemplo : « sozinhos e sozinhas ») ;
- a utilização de flexão dupla (exemplo : « as candidatas e candidatos ») ;
- a utilização da regra de proximidade e/ou do número pela concordância.
Se alguém quiser ir mais longe ainda na inclusão e queira se voluntariar para manter uma versão da carta com uma escritura diferente, nós ficaremos contentes de acolher a vossa contribuição.
Você poderá também nos assinalar suas observações sobre essa questão no nosso [forum](https://forum.chatons.org/t/revision-de-la-charte-ecriture-inclusive/).


>  **Critérios Necessários :**
> - O CHATON se compromete a agir com respeito e benevolência para com os outros membros do coletivo ;
> - O CHATON se compromete a agir com respeito e benevolência para com os usuários de seus serviços ;


## Hospedeiro (Host)

Um hospedeiro é aqui definido como uma entidade que hospeda os dados fornecidos por terceiros e propõe serviços através do qual transitam ou são armazenados seus dados.
Os usuários aqui denominados « hospedado » são pessoas, físicas ou jurídicas, que têm a possibilidade de criar ou modificar dados na infraestrutura du CHATON.

>  **Critérios necessários :**
> - o CHATON deverá ter os acessos como administrador (acesso root) do sistema operacional fazendo funcionar os serviços finais online ;
> - no caso em que o servidor seja locado (virtual ou dedicado), o CHATON deverá assegurar que o fornecedor se compromete contratualmente a não ter acesso aos dados ;
> - o CHATON deverá informar seus visitantes e usuários de grau de controle que ele tem sobre a sua infraestrutura ;
> - o CHATON se compromete a exibir publicamente e sem ambiguidade o seu nível de controle sobre os equipamentos (hardware) e os aplicativos (software) que hospedam os serviços e os dados associados. Nomeadamente, o nome do provedor de hospedagem física dos servidores deverá estar claramente indicado aos hospedados ;
> - o CHATON se compromete a evitar a utilisação de serviços de fornecedores - informáticos ou não - em que as práticas sejam incompatíveis com os princípios da presente carta, especialmente em termos de preservação e de captação de dados privados. Se aplicável, o CHATON informará seus usuários.
>
> **Critérios recomendados :**
> - o CHATON deverá ter o controle total de sua infraestrtura técnica (servidor e rede) de aplicativos e de dados associados (acesso root, incluído hypervisor). Ele deverá ser  único a ter acesso técnico aos dados.


## Alternativas

Ao propor serviços livres online, o CHATON se propôe a ser uma alternativa ao modelo de empresas que propõem serviços fechados e centralizados com fins de monopólio e uso desviado de dados pessoais.
Essas alternativas ilustram a diversidade de soluções livres disponíveis e podem ser colocada à disposição a fins pessoais ou coletivos.

>  **Critérios necessários :**
> - o CHATON se compromete a publicar a documentação de sua infraestrutura e de seus serviços ;
> - o CHATON se compromete a não implementar dispositivos de monitoramento do uso dos serviços propostos que não sejam com finalidade de estatística, técnicas ou administrativas ;
> - o CHATON se compromete a priorizar as liberdades fundamentais de seus usuários, principalmente o respeito de suas vidas privadas, em cada uma de suas ações ;
> - O CHATON não deverá, em nenhum caso utilizar os serviços de gestão de publicidade. O patrocínio ou mecenato, através da exibição da identidade de estruturas parceiras (nome, logo, etc) é autorisado, à condição que nenhuma informação de caráter pessoal venha a ser comunicada aos parceiros ;
> - o CHATON nã deverá fazer nenhuma exploração comercial dos dados ou metadados dos hospedados.

## Transparentes

O CHATON assegura aos hospedados que nenhuma uso desleal será feito de seus dados, nem de suas identidades, ou de seus direitos, especialmente através de condições gerais de utilisações claras, concisas e facilmente acessíveis.
É com base no conhecimento de causa que os usuários poderão usar dos servios disponíveis e tomam conhecimento de suas funcionalidades, de suas vantagens, de seus limites e de seus aplicações.
Nenhum dado produzido através desses serviços não pertencem ao CHATON. O CHATON não se dá nenhum direito e nem pratica nenhuma censura, desde que os conteúdos respeitem as Condições Gerais de Utilização.

>  **Critérios necessários :**
> - o CHATON se compromete a ser transparente quanto á sua infraestrutura técnica (distribuição e aplicativos utilisados). Por razões de segurança, algumas informações (número de verão do aplicativo, por exemplo) poderão não ser comunicadas publicamente ;
> - o CHATON se compromete a fazer o seu melhor (obrigação de meios) em termos de segurança de sua infrastrutura ;
> - o CHATON se compromete a estabelecer uma política de cópia de segurana (backup) que lhe pareça ser a mais adaptável e a documentar publicamente ;
> - o CHATON se compromete a publicar em diário de incidentes técnicos dos serviços ;
> - o CHATON se compromete a não se atribuir nenhum direito de propriedade dos conteúdos, dados e metadados produzidos pelos hospedados ou utilisadores ; 
> - o CHATON se compromete a indicar publicamente qual é a sua oferta de serviço, como também as tarifas associadas a estes ; 
> - o CHATON se compromete a publicar as Condições Gerais de Utilisação (CGU) de modo visível, claro, não ambíguo e compreensível para a maioria ;
> - o CHATON se compromete a publicar, no seu CGU, uma cláusula « Dados pessoais e respeito à vida privada » indicando claramente qual é a política do CHATON no que se refere às práticas visadas ;
> - o CHATON se compromete a tornar público seus relatórios de atividades, pelo ao menos a parte que concerne suas atividades como CHATON ;
> - o CHATON se compromete a trazer ao conhecimento dos hospedados as principais informações relativas à sua política de segurança e salvaguarda (desde que, evidentemente, essas informações não afetem a referida política de segurança ) ;
> - o CHATON se compromete a comunicar com os hospedados a respeit das dificuldades que ele encontrar na exploração de sua estrutura e dos dierentes serviços que ele coloca à disposição, principalmente através da implementação de serviços de suporte e incidentes. 
> 
> **Critérios recomendados :**
> - o CHATON se compromete a publicar estatísticas anônimas de uso ;
> - o CHATON se compromete a tornar público suas contas, pelo ao menos no que se refere à sua atividade de CHATON.


## Abertos (Open) 

A utilização de aplicativos livres (free software) e de padrões abertos (open source) sobre a internet é o meio exclusivo pelo qual os membros propõem seus serviços. O acesso ao código fonte está no fundamento dos princípios do Livre. Por cada serviço que ele propõe, o CHATON se compromete a utilizar de aplicativos (software) ou de elementos de código que estejam exclusivamente sob a licença livre. 
No cas de amelioração do código do aplicativo utilisado, o CHATON se compromete a colocar suas contribuições sob licença livre (compatível com a licença inicial) e encorajará toda contribuição voluntária da parte dos usuários convidando-os a contatar os respectivos criadores. O CHATON se compromete a tornar acessível o código fonte seja publicando um link para o site oficial do aplicativo, seja, caso este último não esteja mais disponível, publicando o código utilizado.

>  **Critérios necessários :**
> - o CHATON se compromete a utilizar exclusivamente aplicativos sob licenças livres, dentro do definido pela Fundação de Software livre (Free Software Foundation), que estejam compatíveis ou não com a licença GPL. Ao que se refere aos microcódigos de hardware para os quais não há alternativa livre funcionais, os software de primeiro nível (BIOS), assim como todo componente de hardware ou software aos quais o CHATON não tenha acesso, este último se compromete a difundir publicamente a lista, assim como seus objetos.
> - o CHATON se compromete a utilizar exclusivamente distribuições livres (GNU/Linux, FreeBSD, etc.) como sistema operacional para a infraestrtura dos hospedados ;
> - o CHATON se compromete, sobre demanda, a fornecer a lista de pacotes instalados no servidor que hospedam os serviços fornecidos aos usuários ;
> - o CHATON se compromete a utilizar apenas formatos abertos nos exercícios de sua atividade de hosepdagem ;
> - o CHATON se compromete, se ele modificar o código fonte do aplicativo utilisado, a tornar público essas modificações ; 
> - o CHATON se compromete a contribuir, tecnicamente, finaceiramente ou de uma outra maneira e na medida de suas capacidades, ao movimento do software livre ;
> - o CHATON se compromete a facilitar a possibilidade para seus hospedados a deixar de utilizar seus serviços levando os dados associados a estes em formatos abertos ;
> - o CHATON se compromete a suprimir definitivamente todas informações (contas e dados pessoais) do hospedado mediante o pedido deste último dentro do limite de suas obrigações legais e técnicas indicadas anteriormente no seu CGU. 
> 
> **Critérios recomendados :**
> - Para aplicativos ligados ao hardware (BIOS, firmware, drivers, etc ), le CHATON se compromete a utilizar exclusivamente aqueles sob licenças livres ;
> - o CHATON se compromete a listar publicamente suas contribuições ao Livre.


## Neutros

A ética do software livre é feita do compartilhamento e da independência. O CHATON se compromete a não praticar nenhuma censura de antemão quanto ao conteúdo, nenhuma vigilância das ações dos usuários, e à não responder a nenhum pedido administrativo ou de autoridade sem um pedido legal apresentado de forma justa e de forma devida.
A igualdade de acesso à suas aplicações é um comprometimento forte : seja ele pagante ou gratuito, as ofertas dos CHATONS devem ser feitas sem discriminação técnica ou social, e oferecer a todos a garantia da neutralidade referente à circulação de dados.
O respeito à vida privada ds usuários é uma necessidade imperativa. Nenhum dado pessoal será explorado com fins comerciais, transmitido a terceiros, ou utilizados com fins não previstos pela presente carta, exceto para necessidades estatísticas e sempre no respeito do quadro legal.

>  **Critérios necessários:**
> - o CHATON se compromete a não praticar nenhuma vigilância de ações dos usuários, senão a fins administrativos, técnicos, ou de melhorias internas dos serviços ; 
> - o CHATON se compromete a não praticar nenhuma censura frente ao conteúdo dos hospedados ;
> - o CHATON se compromete a proteger, da melhor forma possível os dados de seus usuários de violações externas, nomeadamente utilizando,  sempre que possível, uma criptografia forte dos daados, no momento de sua recepção/transmissão na rede (SSL/TLS) ou de seu armazenamento (arquivos ou base de dados, principalmente) ;
 > - o CHATON se compromete a manter uma lista, para seus usuários, de solicitações administrativas ou de autoridade que lhe forem apresentadas. No caso de uma impossibilidade legal de comunicar essas informações, o CHATON é encorajado a implementar um dispositivo de desvio do tipo Warrant Canary ; 
> - o CHATON se compromete a não responder nenhuma solicitação administrativa ou de autoridade necessitando da comunicação de informações pessoais antes que lhe seja apresentado uma solicitação legal justa e de forma devida ;
> - o CHATON se compromete, se uma ação de moderação se fizer necessária, a aplicar seus CGU com benevolência.


## Solidários

Num processo de compartilhamento e de ajuda mútua, cada CHATON difunde aos demais CHATONS e ao público um máximo de conhecimento para promover o uso dos software livre e aprende a instalar serviços livres online. Esse compatilhamento de recursos, técnicas e do pensar congnitivo, faz da internet um bem comum, disponível à todos e não pertence a ninguém. 

>  **Critérios necessários :**
> - o CHATON se compromete à prever estruturamente a possibilidade de encontros pessoais, de troca, e de fazer com que seus usuários participem no limite de suas possibilidades ;
> - o CHATON se compromete a ter uma atitude ativa e voluntária e termos de acesso a todos os serviços propostos, especialmente respeitando as normas de acessibilidade web ;
> - o CHATON se compromete a implementar e promover uma forma de organização e governança inclusiva, capaz de se adaptar às diferenças e a valorizar a diversidade zelando por aqueles que os grupos marginalizam ou excluem sejam elementos chaves no processo de desenvolvimento ;
> - o CHATON se compromente a não excluir a priori os usuários potenciais aos serviços propostos. O CHATON poderá definir o « público alvo » ao qual ele deseja abordar (por exemplo sob o critério de proximidade geográfica ou de centro de interesse). Ele deverá, contudo, dentro do possível, responder à solicitações que não sejam pertinentes, por exemplo, orientando-os a outro CHATON ou facilitando a criação de estruturas que possam responder às necessidades não satisfeitas ;
> - o CHATON se compromete a definir um modelo econômico baseado na solidariedade. Em caso de serviços pagos, estes deverão ser rasoáveis e em adequação com os custos de implementação. Além de salário - o equivalente ao de tempo integral, bônus e dividendos inclusos - o mais baixo possível da estrutura não poderia ser inferior à um quarto do salário - em equivalência ao de tempo integral, bônus e dividendos inclusos - ao mais elevado da estrutura ;
> - o CHATON se compromete a difundir, o mais amplamente possível, publicamente e sob licença livre, os seus conhecimentos. Uma atenção particular será dada àqueles que « não técnicos » a fim de permitir aumentar seus conhecimentos e competências e de poder desfrutar plenamente de seus papeis como colaboradores e colaboradoras ;
> - o CHATON se compromete a facilitar a emancipação do público que ele deseja tocar, nomeadamente através de iniciativas de educação popular (eventos, reencontros, formações internas ou externas, ateliers, tempo de redação colaborativa de documentação, etc.) afim de certificar-se que a Internet permaneça uma tecnologia acessível à todo mundo.

