# Manifeste du CHATONS

## Préambule

Des spectres hantent nos vies numériques : la concentration des services, l'insécurité de nos données personnelles, les menaces sur la liberté d'expression et le partage de l'information. 

Soumises à l'espionnage des multinationales et des États, les informations et les traces des comportements des citoyens sur Internet sont utilisées à des fins de publicité ou de surveillance de masse. Quelles que soient les justifications de ces usages, il est indispensable de préserver des espaces de communication privés, garanties de nos libertés, à commencer par la plus précieuse d'entre elles, la liberté de communiquer, sans entraves. 

Un petit nombre d'acteurs économiques d'Internet ont développé au fil des années des situations de monopole impliquant une centralisation des services. Ce manque de diversité pose la question de la souveraineté des pays et des peuples en créant des inégalités d'accès à l'information, des censures inacceptables, des collectes d'informations personnelles, le tri sélectif des informations pour faciliter l'envahissement de la publicité et du marketing. 

Extensions de nous-mêmes, nos données personnelles indiquent qui nous sommes, nos orientations politiques et sexuelles, nos sujets de prédilections, nos rêves et nos objectifs. Éléments essentiels de la vie privée des individus, l'accès à ces informations doit relever uniquement de la bonne volonté de chacun à diffuser ce qu'il veut, quand il le veut et en connaissance de cause. Le respect de la vie privée est aujourd'hui en grand danger tant du point de vue de la protection contre les malveillances que du point de vue politique, législatif et économique. 

L'accès à l'information et la liberté de communiquer reposent sur le postulat que les supports de l'information doivent être à tout moment vérifiables et pérennes. En cela les libertés qui définissent le logiciel libre sont des corollaires essentiels aux libertés des utilisateurs et des citoyens. Protéger ces libertés revient à : 
  - assurer la pérennité des informations en utilisant des formats libres ;
  - décentraliser les services en multipliant les solutions logicielles accessibles et fiables reposant sur des logiciels libres ;
  -  permettre à tous de ne pas subir le joug de clauses abusives qui enferment les données dans des systèmes irrespectueux de la vie privée ;
  - informer les utilisateurs sur tous les risques qui menacent nos libertés et leur permettre de s'en prémunir.

# 1. Les CHATONS, Collectif d’Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires

Impulsé en 2014, le projet « Dégooglisons Internet » de l'association Framasoft a démontré, en pratique, qu'il était possible de rassembler des compétences et des dispositifs libres et éthiques en vue de décentraliser Internet et aider les individus et les organisations à le faire pour eux-même en diffusant une information appropriée.

Cette expérience, d'autres communautés de passionnés l'avaient depuis longtemps éprouvée avec différents projets. Ils ont favorisé à la fois l'émergence de nouveaux services reposant sur des solutions libres, et l'innovation associée en matière de programmation et d'ingénierie. Ainsi, il est apparu tout un tissu d'initiatives qui offrent aux utilisateurs autant de possibilités de reprendre le contrôle de leurs données, de sauvegarder autant que faire se peut leur vie privée et même de contribuer à leur tour à l'éclosion d'un monde libre et solidaire, objectif qui a toujours bercé l'histoire d'Internet et des hackers depuis plus de 40 ans.

Il est aujourd'hui proposé aux hébergeurs qui adhèrent aux principes du logiciel libre et accordent une importance fondamentale aux libertés individuelles et sociales de participer au collectif CHATONS : Collectif d’Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires.

L'objectif de ce collectif est de mailler les initiatives de services basés sur des solutions de logiciels libres et proposés aux utilisateurs de manière à diffuser toutes les informations utiles permettant au public de pouvoir choisir leurs services en fonction de leurs besoins, avec un maximum de confiance vis-à-vis du respect de leur vie privée, sans publicité ni clause abusive ou obscure.


# 2. Les engagements

Les membres du collectif s’engagent à respecter la charte du collectif, dont les principes sont les suivants.

## 2.1 Transparence, non-discrimination et données personnelles 

La probité est le maître-mot de ces engagements, selon différents points visant à asseoir la fiabilité des services proposés et la confiance des utilisateurs dans ces derniers.
Les conditions générales d'utilisation (CGU) doivent être parfaitement claires, accessibles et non contradictoires avec la charte des CHATONS.

L'hébergeur doit assumer et afficher une politique ouverte de gestion des comptes utilisateurs&nbsp;: sans discrimination, que l'accès soit gratuit ou payant, et dans le respect de la juridiction du pays concerné.

L'hébergeur s'engage à laisser la possibilité pour tous les utilisateurs de pouvoir récupérer leurs données personnelles, chiffrées ou non, sauf dans le cas de services particuliers reposant sur le transfert éphémère et chiffré d'informations personnelles.


## 2.2 Ouverture, économie, protection

Les services proposés doivent satisfaire à quelques exigences techniques. Ainsi, les serveurs doivent reposer sur des solutions logicielles libres. Ces logiciels permettront par ailleurs de rendre possible la reproductibilité du service sans générer de développements supplémentaires quant à la structure du serveur, ou alors à titre de contributions à ces logiciels libres.

L'utilisation des formats ouverts est obligatoire, à minima concernant toutes les données diffusées à destination des utilisateurs. Cela suppose une politique affirmée en faveur de l'interopérabilité. Ainsi, lorsque l'utilisation de formats ouverts est impossible (par exemple s'il faut télécharger un programme à installer sur un système d'exploitation privateur), les données doivent être sous licence libre et disponibles pour un maximum de systèmes d'exploitation. Les sources doivent être rendues accessibles.

Les membres du CHATONS s'engagent à respecter les termes des licences libres des logiciels qu'ils utilisent (y compris mentionner ces licences, renvoyer vers les sources, etc.).

En matière d'éthique, le sponsoring est accepté, ainsi que le mécénat, le don, ou le fait d'avoir un modèle économique consistant à faire payer des fonctionnalités ou même tout le service. Le modèle économique de chaque membre du CHATONS doit être clairement exprimé sur une page dédiée que l'utilisateur peut facilement consulter et comprendre. Évidemment, les aspects économiques de l'activité de chaque membre du CHATONS doivent être rigoureusement en conformité avec la législation du pays concerné.

En revanche, il ne sera accepté aucune publicité en provenance de régies publicitaires. À ce titre, aucune exploitation des données personnelles ne sera accomplie, le suivi des actions des utilisateurs ne pourra se faire qu'à des fins uniquement statistiques et légales, les adresses des utilisateurs ne peuvent être utilisées qu'à des fins administratives ou techniques. Les outils statistiques devront, eux aussi, être libres et satisfaire aux conditions du Collectif.

Si l'hébergeur propose un service ou des fonctionnalités moyennant une contribution de la part de l'utilisateur, le fait de proposer des services libres, reposant sur des logiciels libres, ne doit pas être employé uniquement comme un argument commercial, mais plutôt comme un argument éthique. De même, la fonctionnalité consistant à chiffrer les données ne peut pas être considérée comme une option payante : le chiffrement est l'un des éléments clé de la protection de la vie privée et de la liberté de communiquer : à ce titre il est à considérer comme un droit, et non comme un bien. Si l’hébergeur a la possibilité de  chiffrer (et donc de protéger) les données des utilisateurs, il a le devoir de le proposer.


## 2.3 Solidarité et essaimage

Les membres du CHATONS se doivent entraide et assistance, par une liste de discussion dédiée ou par tout autre moyen à leur disposition, y compris la tenue d'assises ou de réunion périodiques. C'est ainsi que les membres du CHATONS pourront faire progresser leurs services. Un des moyens les plus efficaces de maintenir cette entraide systématique est de contribuer au développement des logiciels libres utilisés.

Les membres du CHATONS ne doivent cependant pas demeurer dans un entre-soi qui ne satisfera qu'un nombre limité de personnes, générateur de discrimination dans l'accès aux services. Au contraire, tous les efforts de communication envers le public sont encouragés de manière à essaimer les solutions d'hébergement libres et créer du lien autour des principes défendus par le collectif. Les moyens doivent être mutualisés et peuvent passer par des formations, des séances d'information publiques, la tenue de stands lors de diverses manifestations, des interventions lors de conférences, la publication de plaquettes, etc.

## 2.4 Neutralité

Les services d'un membre du CHATONS ne pourront être hébergés par un acteur qui, par réputation, ne favorise pas la neutralité du réseau.

Les paquets de données doivent transiter sur les services du membre du CHATONS sans discrimination, ce qui signifie qu'il ne doit pas en examiner le contenu, la source ou la destination.

Aucun protocole de communication ne pourra être privilégié dans le mode de distribution des informations. Et aucune donnée ne pourra voir son contenu arbitrairement altéré.

Toute atteinte aux principes de la neutralité du réseau, soit par un membre du collectif soit par une volonté extérieure affectant un membre ou le collectif dans son ensemble, devra être reportée immédiatement au sein du collectif de manière à y apporter une solution au plus vite. Si elle doit être publiquement dénoncée, cela ne pourra se faire au nom du Collectif qu'après la validation d'un vote qui engage ses membres.

La neutralité du CHATONS est aussi une neutralité politique dans la mesure où les convictions de chaque membre ne seront ni examinées ni sanctionnées tant qu'elle n'outrepassent pas le cadre législatif en vigueur. 


# 3. Politique générale

## 3.1 Relations publiques

Le collectif n'ayant aucun statut officiel, nul ne pourra se prévaloir de parler en son nom sans l'approbation préalable de la majorité simple de ses membres. En revanche, chacun est encouragé à faire connaître le collectif.

On attachera toutefois une importance à l'emplacement géographique et les principales caractéristiques des membres du collectif sur le domaine chatons.org qui sera le principal point de référencement.

Au besoin, le collectif pourra s'exprimer (collectivement) par l'intermédiaire de communiqués diffusés sur le domaine chatons.org : arrivée d'un nouveau membre, prise de position sur un sujet d'actualité, etc.

Un logo sera disponible, si les membres le plébiscitent, et chacun sera libre de l'utiliser ou non sur le site web de sa structure.

## 3.2 Structures des membres

Les membres du CHATONS peuvent être des associations, des particuliers, des entreprises, des collectifs (liste non exhaustive). Pour l'essentiel :

  * chaque membre devra nommer un unique référent (et informer des éventuels changements) qui sera tenu comme le principal point de contact pour les autres membres. Il peut s'agir d'une seule personne ou d'un groupe de personnes, pourvu qu'il soit facile et évident de nouer le contact (par exemple, une seule adresse courriel).
  * chaque membre du collectif diffusera à minima une page web présentant ses services accessibles au public. C'est à cette adresse que fera référence la liste des membres du Collectif.

## 3.3 Fonctionnement du collectif CHATONS

Le collectif CHATONS possède un mode de gouvernance qui s'inspire directement du Logiciel Libre. Les décisions concernant l'évolution du collectif et de la charte seront prises de manière collégiale. À la manière d'un code source, le modèle du collectif pourra être dupliqué et modifié pour qu'il puisse s'adapter par exemple à des particularités régionales.

Chaque membre est invité à participer aux décisions collectives qui se prendront, autant que faire se peut, de façon consensuelle. En cas de conflit d'opinion, les décisions pourront être prises à la majorité simple.

Le domaine chatons.org est maintenu et hébergé par Framasoft (tant que cela sera possible et sauf si le collectif en juge autrement). Il comportera un site web, comprenant la liste des membres, ainsi qu'une liste de diffusion permettant d'animer le collectif par l'échange entre ses membres. Ces derniers seront invités à collaborer aux contenus diffusés par ce site, de manière à communiquer publiquement des informations relatives au CHATONS et à l'hébergement libre.

Il n'existe aucun statut administratif du CHATONS qui est principalement une liste publique recensant ses membres ainsi qu'un ensemble de documentations visant à faciliter l'échange de savoirs, la capitalisation de bonnes pratiques, et l'essaimage. 

Toute organisation respectant les principes du présent manifeste et de la charte du CHATONS peut prétendre à en devenir membre. Pour être maintenue en tant que membre, elle devra communiquer au collectif les informations concernant son point de contact et inscrire au moins un de ses propres membres sur la liste de discussion. 

Après discussion et éventuellement quelques conseils proposés, un vote à la majorité simple pourra avoir lieu concernant l'acceptation de cette nouvelle organisation par le collectif. 

Un ou plusieurs membres peuvent toutefois se réserver le droit de réclamer le retrait d'un autre membre à condition&nbsp;:

  * d'étayer la proposition à l'aide d’arguments probants diffusés à l'ensemble des membres ;
  * d'accepter un vote collectif, avec ou sans débat contradictoire.

Conscient qu'il n'est pas possible de garantir le respect de l'ensemble des points de la Charte des CHATONS sans nuire à la confidentialité des données personnelles hébergées sur les systèmes informatiques des membres, le contrôle entre pairs sera *de facto* imparfait. Le collectif repose donc avant tout sur la confiance et la bienveillance que s'accordent les membres entre eux.

Les CHATONS devront donc trouver, entre eux, et dans le respect des points de vue chacun, les bonnes pratiques et règles de gestion d'inclusion, mise en cause ou exclusion des membres du collectif, dans l'intérêt prioritaire du respect des libertés fondamentales et de la vie privées des utilisateurs des services du collectif.